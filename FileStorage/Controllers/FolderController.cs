﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileStorage.DAL;
using FileStorage.Models;
using WebGrease.Css;
using File = FileStorage.Models.File;
using StdFile = System.IO.File;


namespace FileStorage.Controllers
{
    /// <summary>
    /// This is the main Controller class for the App
    /// </summary>
    public class FolderController : Controller
    {
        /// <summary>
        /// The Database accessor
        /// </summary>
        private FileContext db = new FileContext();


        /// <summary>
        /// The controller for the default page "/"
        /// </summary>
        /// <returns>
        /// The "Folder/Index.cshtml" View
        /// </returns>
        public ActionResult Index()
        {
            var f = db.Folders.Find(1);
            ViewData["Folders"] = f.ContainedFolders;
            ViewData["Files"] = f.ContainedFiles;
            ViewData["FolderID"] = 1;
            return View();
        }


        /// <summary>
        /// The controller for the "/Open" Page
        /// </summary>
        /// <remarks>
        /// This page is for viewing any other folder than the root folder (With FolderID of 1)
        /// </remarks>
        /// <param name="FolderID">
        /// The FolderID parameter is used to pull the specified folder from the DB, if no paramater is passed the controller defaults to sending the user to "/"
        /// </param>
        /// <returns>The "Folder/Open.cshtml" View </returns>
        [HttpGet]
        public ActionResult Open(int? FolderID)
        {
            if (!FolderID.HasValue)
                return RedirectToAction("Index");

            var f = db.Folders.Find(FolderID);
            ViewData["Folders"] = f.ContainedFolders;
            ViewData["Files"] = f.ContainedFiles;
            ViewData["FolderID"] = FolderID;
            ViewData["FolderName"] = f.FolderName;
            ViewData["FolderPath"] = f.FolderPath;
            return View();
        }


        /// <summary>
        /// The GET Method Controller for the "/UploadFile" Page
        /// </summary>
        /// <param name="FolderID">
        /// The FolderID is only passed to the page to be passed back in the POST method
        /// </param>
        /// <returns>The "Folder/UploadFile.cshtml" View </returns>
        [HttpGet]
        public ActionResult UploadFile(int? FolderID)
        {
            ViewData["FolderID"] = FolderID;
            return View();
        }


        /// <summary>
        /// The POST Method Controller for the "/UploadFile" page
        /// </summary>
        /// <param name="fileModel">
        /// The parameter fileModel <see cref="FileViewModel"/> contains File information and a FolderID.
        /// The FolderID is where the File is saved, and the File contents is converted into a byte array stored in the File.FileContents property
        /// The File.FileName is also checked against the DB to see if an existing file with a matching FileName exists, if so an Error is returned.
        /// </param>
        /// <returns>A redirect to either the folder that was uploaded to or the error page</returns>
        [HttpPost]
        public ActionResult UploadFile([Bind(Include = "FolderID,File")]FileViewModel fileModel)
        {
            if (ModelState.IsValid)
            {
                // Prepare new MemoryStream Object
                var fileData = new MemoryStream();
                // Copy Filde Data to MemoryStream
                fileModel.File.InputStream.CopyTo(fileData);

                var folder = db.Folders.Find(fileModel.FolderID); // Find Folder with ID 'FolderID'
                if (folder != null) // If folder exists:
                {
                    // If Folders contained Files does not contain a file with the same name:
                    if (folder.ContainedFiles.Count(f => f.FileName == fileModel.File.FileName) == 0)
                    {

                        var file = new File
                        {
                            FileName = fileModel.File.FileName, FileContent = fileData.ToArray(),
                            FolderID = folder.FolderID, ContentType = fileModel.File.ContentType,
                            ContentLength = fileModel.File.ContentLength
                        };
                        // Add File to DB Table
                        db.Files.Add(file);

                        // Add File to Folder.Contained Files
                        folder.ContainedFiles.Add(file);
                        db.SaveChanges();
                        // If Success
                        return RedirectToAction("Index", new {id = fileModel.FolderID});
                    }
                    else
                    {
                        // If Another file with the specified name exist, Error
                        return RedirectToAction("Error", new {ErrorMsg = "Duplicate File Name Found..."});
                    }
                }
                else
                {
                    // If Folder with 'FolderID' doesn't exist, Error
                    return RedirectToAction("Error", new { ErrorMsg = "Folder Not Found..." });
                }
            }
            else
            {
                // If ModelState Is Invalid, Error
                return RedirectToAction("Error", new { ErrorMsg = "ModelState Invalid" });
            }
            
        }



        /// <summary>
        /// The GET Method Controller for the "/ViewFile" page
        /// </summary>
        /// <remarks>
        /// This page is a download factory
        /// </remarks>
        /// <param name="FileID">
        /// FileID is used to Find the corresponding file and stream it back to the user as a MemoryStream type
        /// </param>
        /// <returns>
        /// A MemoryStream or a redirect to the Error page if the file could not be found
        /// </returns>
        [HttpGet]
        public ActionResult ViewFile(int? FileID)
        {
            var file = db.Files.Find(FileID);
            if (file != null) // If Files exists
            {
                var fs = new MemoryStream(file.FileContent);
                // Send data as File
                return File(file.FileContent, System.Net.Mime.MediaTypeNames.Application.Octet, file.FileName);

            }
            else
            { // If File does not exist
                return RedirectToAction("Error", new { ErrorMsg = "FileNotFound Error" });
            }
        }


        /// <summary>
        /// The GET Method Controller for the "/NewFolder" Page
        /// </summary>
        /// <param name="FolderID">
        /// The FolderID is only passed to the page to be passed back in the POST method
        /// </param>
        /// <returns>The "Folder/NewFolder.cshtml" View </returns>
        [HttpGet]
        public ActionResult NewFolder(int? FolderID)
        {
            ViewData["FolderID"] = FolderID;
            return View();
        }


        /// <summary>
        /// The POST Method Controller for the "/NewFolder" page
        /// </summary>
        /// <param name="folderModel">
        /// The parameter folderModel <see cref="FolderViewModel"/> contains a FolderName (string) and a FolderID (int).
        /// The FolderID is where the Folder is created, with FolderName as its name.
        /// The Folder.FolderName is also checked against the DB to see if an existing Folder with a matching Folder exists, if so an Error is returned.
        /// </param>
        /// <returns>A redirect to either the folder that was uploaded to or the error page</returns>
        [HttpPost]
        public ActionResult NewFolder([Bind(Include = "FolderID,FolderName")]
            FolderViewModel folderModel)
        {
            var parentFolder = db.Folders.Find(folderModel.FolderID);
            if (parentFolder != null) // If Folder Exists
            {
                if (parentFolder.ContainedFolders.Count(f => f.FolderName == folderModel.FolderName) == 0) // If newFolder doesn't share it's name with other folders in this directory
                {
                    var newfolder = new Folder { FolderName = folderModel.FolderName, FolderPath = parentFolder.FolderPath + folderModel.FolderName + "/" };
                    db.Folders.Add(newfolder);
                    parentFolder.ContainedFolders.Add(newfolder);
                    db.SaveChanges();
                }
                else
                {   // Duplicate Folder Name found
                    return RedirectToAction("Error", new {ErrorMsg = "Duplicate Folder Name..."});
                }
                
                // Return Success
                return RedirectToAction("Open", new {FolderID=parentFolder.FolderID});
            }
            else
            {
                // Folder not found
                return RedirectToAction("Error", new { ErrorMsg = "FolderNotFound Error" });
            }
        }


        /// <summary>
        /// The GET Method for the "/Error" page
        /// </summary>
        /// <param name="ErrorMsg">
        /// This is the message printed on the page
        /// </param>
        /// <returns>
        /// The "Folder/Error.cshtml" View
        /// </returns>
        [HttpGet]
        public ActionResult Error(string ErrorMsg)
        {
            ViewData["ErrorMsg"] = ErrorMsg;
            return View();
        }


        /// <summary>
        /// The POST Method for the "/Search" page
        /// </summary>
        /// <param name="searchViewModel">
        /// The parameter searchViewModel <see cref="SearchViewModel"/> contains two properties: Query (string) and FilterType (string)
        /// </param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Search([Bind(Include = "Query,FilterType")]SearchViewModel searchViewModel)
        {
            // Init _files & _folders, these will be passed to the browser if they are populated or not
            List<File> _files = new List<File>();
            List<Folder> _folders = new List<Folder>();



            if (searchViewModel.FilterType == "Any") // Any selection from the search dropdown
            {
                // Perform a "LIKE" query on the files
                var file = db.Files.Where(f =>
                        SqlFunctions.PatIndex(
                            "%" + searchViewModel.Query + "%",
                            f.FileName) > 0)
                    .ToArray();
                // Add the result to _files
                _files.AddRange(file);

                // Perform a "LIKE" query on the folders
                var folders = db.Folders.Where(f =>
                        SqlFunctions.PatIndex(
                            "%" + searchViewModel.Query + "%",
                            f.FolderName) > 0)
                    .ToArray();
                // Add the result to _folders
                _folders.AddRange(folders);

                ViewData["Files"] = _files;
                ViewData["Folders"] = _folders;
                return View();
                // Send Data and View to user


            } else if (searchViewModel.FilterType == "FileID")
            {
                int id;
                try
                {
                    id = Int32.Parse(searchViewModel.Query);

                    var file = db.Files.Find(id); // Find File with ID
                    if (file != null) // If file exists
                    {

                        _files.Add(file);
                        ViewData["Files"] = _files;
                        ViewData["Folders"] = _folders;
                        return View();
                    }
                    else
                    {
                        ViewData["Files"] = _files;
                        ViewData["Folders"] = _folders;
                        return View();
                    }
                }
                catch (FormatException exception)
                {
                    RedirectToAction("Error", new { ErrorMsg = exception.Message }); // int32 Parse Error
                }
            } else if (searchViewModel.FilterType == "FileName")
            {
                // Perform a "LIKE" query on the files
                var file = db.Files.Where(f => 
                        SqlFunctions.PatIndex(
                            "%" + searchViewModel.Query + "%", 
                            f.FileName) > 0)
                    .ToArray();
                if (file.Length != 0) // If file is not empty
                {

                    _files.AddRange(file);
                    // Add file range to _files
                    ViewData["Files"] = _files;
                    ViewData["Folders"] = _folders;
                    return View();
                }
                else
                {
                    ViewData["Files"] = _files;
                    ViewData["Folders"] = _folders;
                    return View();
                }
            }else if (searchViewModel.FilterType == "FolderName") // If Query is FolderName
            {
                // Perform a "LIKE" query on the folders
                var folders = db.Folders.Where(f =>
                        SqlFunctions.PatIndex(
                            "%" + searchViewModel.Query + "%",
                            f.FolderName) > 0)
                    .ToArray();
                
                
                if (folders.Length != 0) // if folders is not empty
                {

                    _folders.AddRange(folders);
                    // Add folders range to _folders
                    ViewData["Files"] = _files;
                    ViewData["Folders"] = _folders;
                    return View();
                }
                else
                {
                    ViewData["Files"] = _files;
                    ViewData["Folders"] = _folders;
                    return View();
                }
            }
            ViewData["Files"] = _files;
            ViewData["Folders"] = _folders;
            return View();
        }
    }
}