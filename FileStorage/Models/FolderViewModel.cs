﻿namespace FileStorage.Models
{
    public class FolderViewModel
    {
        public string FolderName { get; set; }
        public int FolderID { get; set; }
    }
}