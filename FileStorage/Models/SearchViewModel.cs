﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileStorage.Models
{
    public class SearchViewModel
    {
        public string Query { get; set; }
        public string FilterType { get; set; }
    }
}