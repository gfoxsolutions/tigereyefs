﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileStorage.Models
{
    public class FileViewModel
    {
        public HttpPostedFileBase File { get; set; }
        public int FolderID { get; set; }
    }
}