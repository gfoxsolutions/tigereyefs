﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileStorage.Models
{
    public class Folder
    {
        public int FolderID { get; set; }
        public string FolderName { get; set; }
        public string FolderPath { get; set; }

        
        public virtual List<File> ContainedFiles { get; set; }
        public virtual List<Folder> ContainedFolders { get; set; }



    }
}