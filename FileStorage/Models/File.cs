﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace FileStorage.Models
{
   
    public class File
    {
        public int FileID { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public int ContentLength { get; set; }

        public byte[] FileContent { get; set; }

        public int FolderID { get; set; }
        public virtual Folder Folder { get; set; }

    }
}