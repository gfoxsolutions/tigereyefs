﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using FileStorage.Models;

namespace FileStorage.DAL
{
    /// <summary>
    /// FileContext Class provided and interface to the SQL DB
    /// </summary>
    public class FileContext : DbContext
    {
        /// <summary>
        /// Opens the Connection defined in "Web.config"
        /// </summary>
        public FileContext() : base("DefaultConnection")
        {

        }

        public DbSet<Folder> Folders { get; set; }
        public DbSet<File> Files { get; set; }

    }
}